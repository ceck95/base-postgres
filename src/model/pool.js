const config = require('config');
const pgPool = require('pg').Pool;
const BPromise = require('bluebird');
const log = require('core-express').Log;

const pgConfigs = config.get('postgres'),
  pool = {};

const connectPool = pool => {
  return new BPromise((resolve, reject) => {
    // conn.Promise = BPromise;
    pool = new pgPool(pgConfigs);
    // attach an error handler to the pool for when a connected, idle client
    // receives an error by being disconnected, etc
    pool.on('error', error => {
      log.error('Initializes postgres pool failed', error);
      return reject(error);
    });
    log.info('Creates postgres pool successfully');

    return resolve(pool);
  });
};

connectPool(pool);

module.exports = pool;
